#!/usr/bin/env python3
import argparse
from subprocess import call

parser = argparse.ArgumentParser(description='generate program or flash a microcontroller')
parser.add_argument('--flash', action='store_true', help='flash the controller')
parser.add_argument('--freq', type=int, help="output frequency, 80, 100 or 125", default=100)

args = parser.parse_args()

CFLAGS = "-Os -ggdb3 -Wall -Wextra -std=gnu++17 -fno-exceptions -fno-rtti -mno-long-calls -fpic "
LDFLAGS = "-static -nostartfiles -ffreestanding "
LDLIBS = "-Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group "

# generate a linker script
call('arm-none-eabi-gcc -D_ROM=16K -D_RAM=4K -D_ROM_OFF=0x08000000 -D_RAM_OFF=0x20000000 -P -E ' +
     '$OPENCM3_DIR/ld/linker.ld.S -o lscript.ld',
     shell=True)

compilecmd = 'arm-none-eabi-g++ ' + CFLAGS + '-DSTM32F0 '
compilecmd += '-DSTM32F030F4P6 -I$OPENCM3_DIR/include '
compilecmd += '-mcpu=cortex-m0 -mthumb -msoft-float '
compilecmd += '-o clockgen.elf clockgen.cpp ' + LDLIBS
compilecmd += '-lopencm3_stm32f0 ' + LDFLAGS + '-L$OPENCM3_DIR/lib -Tlscript.ld '

print(compilecmd)

call(compilecmd, shell=True)

call('size clockgen.elf', shell=True)

if args.flash:
    call('openocd -f flash.cfg', shell=True)
