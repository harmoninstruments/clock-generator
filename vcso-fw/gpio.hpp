#pragma once
// Copyright (C) 2014 - 2019 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include <cstdint>

enum class GPIOPort : uint32_t {
        A = 0x48000000,
        B = 0x48000400,
        C = 0x48000800,
        D = 0x48000C00,
        E = 0x48001000,
        F = 0x48001400,
};

enum class GPIOMode { in, out, alt, analog };
enum class GPIOSpeed { slow, medium, undefined, fast };

template <GPIOPort port, unsigned pin> class IOPin {
public:
        enum class Regs {
                MODE,
                OTYPE,
                OSPEED,
                PUPD,
                ID,
                OD,
                BSR,
                LCK,
                AFRL,
                AFRH,
                BR
        };
        uint32_t read(const Regs r) {
                asm volatile("" : : : "memory");
                auto regs = reinterpret_cast<uint32_t *>(port);
                return regs[static_cast<uint32_t>(r)];
        }
        void write(const Regs r, const uint32_t d) {
                uint32_t *regs = reinterpret_cast<uint32_t *>(port);
                regs[static_cast<uint32_t>(r)] = d;
                asm volatile("" : : : "memory");
        }

        constexpr IOPin() { static_assert(pin < 16, "IOPin number invalid"); }
        void set() { write(Regs::BSR, (1 << pin)); }
        uint32_t get() { return read(Regs::ID) & (1 << pin); }
        void clear() { write(Regs::BR, (1 << pin)); }
        void clear_multi(uint32_t v) { write(Regs::BR, (v << pin)); }
        void set_multi(uint32_t v) { write(Regs::BSR, (v << pin)); }
        void write_bsr(uint32_t v) { write(Regs::BSR, v); }
        void mode(const GPIOMode m) {
                uint32_t mr = read(Regs::MODE);
                mr &= ~(3 << (2 * pin));
                mr |= static_cast<uint32_t>(m) << (2 * pin);
                write(Regs::MODE, mr);
        }
        void speed(const GPIOSpeed s) {
                uint32_t sr = read(Regs::OSPEED);
                sr &= ~(3 << (2 * pin));
                sr |= static_cast<uint32_t>(s) << (2 * pin);
                write(Regs::OSPEED, sr);
        }
        constexpr unsigned get_pin() { return pin; }
};
