# Clock Generator

## VCXO

A simple VCXO + PLL clock generator for test setups. Accepts a 10 MHz reference. Output frequencies of 80, 100 or 125 MHz.

![photo of PCB](docs/clockgen.jpg)

## VCSO 1 GHz

### Unit 1
 - 2 unfiltered SMA outputs: 17.2 dBm
 - 1 filtered SMA output: 16.25 dBm
 - supply current: 163 mA
 - tuning voltage 550 mV before removal of VCSO shield