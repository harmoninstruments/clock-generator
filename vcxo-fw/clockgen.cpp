// Harmon Instruments VNA
// Copyright (C) 2014 - 2020 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include "gpio.hpp"

static IOPin<GPIOPort::A, 5> sck{};
static IOPin<GPIOPort::A, 6> ce{};
static IOPin<GPIOPort::A, 7> sdata{};
static IOPin<GPIOPort::B, 1> cs{};

#include <cstdint>
#include <cstdlib>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

class Timer {
private:
        uint32_t begin;

public:
        Timer() { reset(); }
        void reset() { begin = STK_CVR; }
        uint32_t elapsed() {
                __sync_synchronize();
                return 0x00FFFFFF & (begin - STK_CVR);
        }
};

constexpr unsigned timer_ms = 8000;
static void sleep_cycles(unsigned d) {
        Timer t;
        while (t.elapsed() < d)
                ;
}

static void pll_spi(uint32_t d) {
        cs.clear();
        for (int i = 0; i < 24; i++) {
                if (d & (1 << 23))
                        sdata.set();
                else
                        sdata.clear();
                sck.set();
                sck.clear();
                d <<= 1;
        }
        cs.set();
}

int main(void) {
        RCC_AHBENR = RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

        STK_RVR = 0xFFFFFF;
        STK_CSR = STK_CSR_CLKSOURCE_AHB | STK_CSR_ENABLE;

        GPIOA_MODER = 0x28000000 | (1 << 14) | (1 << 12) | (1 << 10) | (3 << 8);
        GPIOB_MODER = 1 << 2;
        __sync_synchronize();

        cs.set();
        sck.clear();
        ce.clear();

        sleep_cycles(timer_ms * 100);

        int r = R;
        int n = N;
        int cur = CUR;  // 0 (0.6 mA) - 7 (5 mA)
        int muxout = 1; // 1 = digital lock detect

        // function latch
        pll_spi(2 | (0 << 7) | (cur << 15) | (muxout << 4));
        // R counter
        pll_spi(0 | (r << 2));
        // N counter
        pll_spi(1 | (n << 8));

        ce.set();

        while (1) {
        }
        return 0;
}
